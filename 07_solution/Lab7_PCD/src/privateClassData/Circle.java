package privateClassData;

import java.awt.Color;
import java.awt.Point;

public class Circle {
	private class CircleData {
		private double radius;
		private Color color;
		private Point origin;
		double getRadius() { return radius; }
		Color getColor() { return color; }
		Point getPoint() { return origin; }
		CircleData(double r, Color c, Point o) {
			radius = r;
			color = c;
			origin = o;
		}
	};
	CircleData data;
	public Circle(double radius, Color color, Point origin) {
		data = new CircleData(radius, color, origin);

	}
	public double Circumference() {
		return 2 * Math.PI * this.data.getRadius();
	}
	public double Diameter() {
		return 2 * this.data.getRadius();
	}
	public void Draw() {
		System.out.println("Drawing...\n");
		//I can no longer mess around with things I shouldn't be able to change
		//radius += 1;
	}
	public static void main(String[] args) {
		Circle myc = new Circle(5, new Color(1, 0, 0), new Point(20, 20));
		myc.Draw();
	}

}
