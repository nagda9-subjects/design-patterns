package dp;

public class LegacyLineAdapter implements Shape{
    LegacyLine l;
    public LegacyLineAdapter(LegacyLine l2) {
        l=l2;
    }

    @Override
    public void draw(int x, int y, int x2, int y2) {
        l.draw(x,y,x2,y2);
    }
}
