package interpreter;

public class NotExp extends BooleanExp {
	public NotExp(BooleanExp op1) {
		_operand1 = op1;
	};

	public boolean evaluate(Context aContext) {
		return !_operand1.evaluate(aContext);
	}
	public BooleanExp replace(String name, BooleanExp exp) {
		return new NotExp(_operand1.replace(name, exp));
	}
	
	public BooleanExp copy() {
		return new NotExp(_operand1.copy());
	};
	
	private BooleanExp _operand1;
}
