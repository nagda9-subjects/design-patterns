package interpreter;

public class AndExp extends BooleanExp {
	public AndExp(BooleanExp op1, BooleanExp op2) {
		_operand1 = op1;
		_operand2 = op2;
	};

	public boolean evaluate(Context aContext) {
		return
		        _operand1.evaluate(aContext) &&
		        _operand2.evaluate(aContext);
	}
	public BooleanExp replace(String name, BooleanExp exp) {
		return new AndExp(
		            _operand1.replace(name, exp),
		            _operand2.replace(name, exp));
	}
	
	public BooleanExp copy() {
		return new AndExp(_operand1.copy(), _operand2.copy());
	};
	
	private BooleanExp _operand1;
	private BooleanExp _operand2;
}
