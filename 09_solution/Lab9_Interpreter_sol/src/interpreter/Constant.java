package interpreter;

public class Constant extends BooleanExp {
		public Constant(boolean v) { _value = v; };

		public boolean evaluate(Context aContext) {
			return _value;
		}
		public BooleanExp replace(String name, BooleanExp exp) {
			return new Constant(_value);
		}
		public BooleanExp copy() {
			return new Constant(_value);
		}
		
		private boolean _value;

}
