package interpreter;

public abstract class BooleanExp {
	public BooleanExp() {};

	public abstract boolean evaluate(Context c);
	public abstract BooleanExp replace(String name, BooleanExp exp);
	public abstract BooleanExp copy();
};