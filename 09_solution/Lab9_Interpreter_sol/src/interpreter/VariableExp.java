package interpreter;

public class VariableExp extends BooleanExp {
	public VariableExp(String s) {
		_name = s;
	}
	
	public boolean evaluate(Context a) {
		return a.lookup(_name);
	};
	public BooleanExp replace(String name, BooleanExp exp) {
		if (name.equals(_name)) {
	        return exp.copy();
	    } else {
	        return new VariableExp(_name);
	   }
	}
	public BooleanExp copy() {
		return new VariableExp(_name);
	};
	private String _name;
}
