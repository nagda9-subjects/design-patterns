package observer;

import java.util.ArrayList;

public class Subject {
	protected int m_value;
	protected ArrayList<MyObserver> list = new ArrayList<>();
	public Subject()
	{
	}
	public void set_value(int value)
	{
		m_value = value;
		notifyObservers();
	}
	public void notifyObservers()
	{
		for (MyObserver o : list) {
			o.update(m_value);
		}
	}

	public void addObserver(MyObserver o) {
		list.add(o);
	}

	public void removeObserver(MyObserver o) {
		list.remove(o);
	}

	public static void main(String[] args) {
		Subject subj = new Subject();
		subj.addObserver(new DivObserver(3));
		subj.addObserver(new ModObserver(4));
		subj.set_value(14);
	}

}
