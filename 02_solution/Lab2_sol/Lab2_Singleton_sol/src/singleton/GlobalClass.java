package singleton;

public class GlobalClass
{
    protected int m_value;
  
    protected static GlobalClass global_ptr = null;

    public static GlobalClass Instance() {
        if (global_ptr==null) global_ptr=new GlobalClass();
        return global_ptr;
    }
    private GlobalClass() {
    		m_value = 0;
    }
    public int get_value()
    {
        return m_value;
    }
    public void set_value(int v)
    {
        m_value = v;
    }
};