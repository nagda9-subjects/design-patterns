package iterator;

import java.util.Iterator;

public class MyLinkedListIterator implements Iterator {
    BadLinkedList.Node current;
    public MyLinkedListIterator(BadLinkedList.Node head) {
        current = head;
    }
    @Override
    public boolean hasNext() {
        return current != null;
    }

    @Override
    public Integer next() {
        Integer r = current.element;
        current = current.next;
        return r;
    }
}
