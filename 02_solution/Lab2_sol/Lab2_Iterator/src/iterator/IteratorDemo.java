package iterator;

import java.util.Collection;

public class IteratorDemo
{
    public static void main(String[] args)
    {
        BadLinkedList some_object = new BadLinkedList();
        for (int i = 9; i > 0; --i)
          some_object.insertBack(i);
        MyLinkedListIterator it = some_object.getIterator();
        while (it.hasNext()) {
           System.out.print(it.next() + "  ");
        }
        System.out.println();
    }
}