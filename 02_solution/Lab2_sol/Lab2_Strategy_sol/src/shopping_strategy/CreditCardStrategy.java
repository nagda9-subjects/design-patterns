package shopping_strategy;

public class CreditCardStrategy implements PaymentStrategy {

	private String name;
	private String cardNumber;
	private String cvv;
	private String dateOfExpiry;
	
	public CreditCardStrategy(String nm, String ccNum, String cvv, String expiryDate){
		this.name=nm;
		this.cardNumber=ccNum;
		this.cvv=cvv;
		this.dateOfExpiry=expiryDate;
	}
	@Override
	public void pay(int amount) {
		System.out.println("Contacting bank to pay with card "+cardNumber+" name: "+name+" expiry: "+dateOfExpiry+ " cvv: ***");
		System.out.println(amount +" paid with credit/debit card");
	}

}