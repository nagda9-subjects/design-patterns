package shopping_strategy;

public interface PaymentStrategy {

	public void pay(int amount);
}