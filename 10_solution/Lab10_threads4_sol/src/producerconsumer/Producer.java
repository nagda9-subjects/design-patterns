package producerconsumer;

import java.util.LinkedList;
import java.util.Random;

public class Producer extends Thread {
    public static LinkedList<Object> list = new LinkedList<>();
    public static int fullSize = 10;

    @Override
    public void run() {
        Random generator = new Random();
        while (true) {
            try {
                Thread.sleep(500+generator.nextInt(500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (list) {
                while (list.size() >= fullSize) {
                    try {
                        list.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                list.add(new Object());
                System.out.println("Produced");
                list.notifyAll();
            }
        }

    }

    public static void main(String[] args) {
	    for (int i = 0; i < 10; i++) {
            (new Producer()).start();
            (new Consumer()).start();
        }
    }
}
