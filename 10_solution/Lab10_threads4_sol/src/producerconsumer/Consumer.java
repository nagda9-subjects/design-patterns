package producerconsumer;

import java.util.Random;

public class Consumer extends Thread {
    @Override
    public void run() {
        Random generator = new Random();
        while (true) {
            synchronized (Producer.list) {
                while (Producer.list.size() == 0) {
                    try {
                        Producer.list.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Object o = Producer.list.removeFirst();
                System.out.println("Consumed");
                Producer.list.notifyAll();
            }
            try {
                Thread.sleep(500+generator.nextInt(500));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
