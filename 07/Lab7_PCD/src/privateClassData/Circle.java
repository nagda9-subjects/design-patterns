package privateClassData;

import java.awt.Color;
import java.awt.Point;

public class Circle {
	private double radius;
	private Color color;
	private Point origin;
	public Circle(double radius, Color color, Point origin) {
		this.radius = radius;
		this.color = color;
		this.origin = origin;
	}
	public double Circumference() {
		return 2 * Math.PI * this.radius;
	}
	public double Diameter() {
		return 2 * this.radius;
	}
	public void Draw() {
		System.out.println("Drawing...\n");
		//I can mess around with things I shouldn't be able to change
		radius += 1;
	}
	public static void main(String[] args) {
		Circle myc = new Circle(5, new Color(1, 0, 0), new Point(20, 20));
		myc.Draw();
	}

}
