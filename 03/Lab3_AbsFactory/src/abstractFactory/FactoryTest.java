package abstractFactory;

import java.util.Scanner;

public class FactoryTest {

	public static void main(String[] args) {
		System.out.print("1 - Simple 2 - Robust : ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		scanner.close();
		//Let's create a Rectangular shape:
		Shape rect;
		if (input.equals("1")) {
			rect = new Square();  
		} else {
			rect = new Rectangle();
		}
		rect.draw();
		
		//Now let's create a circular shape:
		Shape circ;
		if (input.equals("1")) {
			circ = new Circle();  
		} else {
			circ = new Ellipse();
		}
		circ.draw();

	}

}
