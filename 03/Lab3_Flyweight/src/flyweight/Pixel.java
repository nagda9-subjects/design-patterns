package flyweight;
public class Pixel {
	private int        row, col;
	public Pixel(int rowid, int colid ) {
		row = rowid;
		col = colid;
	}
	void report() {
		System.out.print( "  " + row + "," + col);
	}
}