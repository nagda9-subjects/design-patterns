package state;

public class TVRemote {
	private static final int TV_OFF = 0, TV_ON = 1;
	private int state = TV_OFF;
	private int volume = 0;
	
	public void pushPower(){
		switch(state){
		case TV_OFF:
			System.out.println("Turn ON the TV");
			state = TV_ON;
			break;
		case TV_ON:
			System.out.println("Turn OFF the TV");
			state = TV_OFF;
			break;
		}
	}
	public void increaseVolume(){
		switch(state){
		case TV_OFF:
			System.out.println("TV is OFF can't increase volume.");
			break;
		case TV_ON:
			volume++;
			System.out.println(String.format("Volume is currently: %d",volume));
			break;
		}
	}

	public void decreaseVolume(){
		switch(state){
		case TV_OFF:
			System.out.println("TV is OFF can't decrease volume.");
			break;
		case TV_ON:
			volume--;
			System.out.println(String.format("Volume is currently: %d",volume));
			break;
		}
	}
}
